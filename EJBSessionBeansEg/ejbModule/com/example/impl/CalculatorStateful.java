package com.example.impl;

import javax.ejb.Stateful;

import com.example.CalculatorLocal;
import com.example.CalculatorRemote;

@Stateful(mappedName="CalStateful")
public class CalculatorStateful implements CalculatorLocal, CalculatorRemote {

	@Override
	public int add(int a, int b) {
		return (a+b);
	} 

}
