package com.example.impl;

import javax.ejb.Stateless;

import com.example.CalculatorLocal;
import com.example.CalculatorRemote;

@Stateless(mappedName="CalStateless")
public class CalculatorStateless implements CalculatorRemote, CalculatorLocal {

	@Override
	public int add(int a, int b) {
		return (a+b);
	}

}
