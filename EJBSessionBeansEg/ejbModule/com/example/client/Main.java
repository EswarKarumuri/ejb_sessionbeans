package com.example.client;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.example.CalculatorRemote;

public class Main {

	public static void main(String[] args) throws NamingException{
		Hashtable<String , String> ht = new Hashtable<String, String>();
		ht.put("java.naming.factory.initial", "weblogic.jndi.WLInitialContextFactory");
		ht.put("java.naming.provider.url", "t3://localhost:7501");
		Context ctx = new InitialContext(ht);
		
		CalculatorRemote calculatorRemoteStateless = (CalculatorRemote) ctx.lookup("CalStateless#com.example.CalculatorRemote");
		System.out.println(calculatorRemoteStateless.add(1, 1));
		
		CalculatorRemote calculatorRemoteStateful = (CalculatorRemote) ctx.lookup("CalStateful#com.example.CalculatorRemote");
		System.out.println(calculatorRemoteStateful.add(2, 1));
	}
}
